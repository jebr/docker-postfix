# Dockerized Postfix SMTP relay based on Alpine Linux

## Build image

* Clone this repo

```bash
docker build -t docker-postfix:1.1 .
```

## Use prebuilt

```bash
docker pull registry.gitlab.com/jebr/docker-postfix:1.1
```

## Run

Required environment variables

* `SMTP_SERVER` - address of the SMTP server to use
* `SMTP_USERNAME` - username to authenticate with
* `SMTP_PASSWORD` - password for the SMTP user
* `SERVER_HOSTNAME` - server hostname for the Postfix container. Emails will appear to come from the hostname's domain
* `RELAY_NETWORKS` - networks (in CIDR format and separated by spaces) that are allowed to relay mail via the SMTP server 

```bash
docker run -d --name postfix -p "25:25"  \
  -e SMTP_SERVER=smtp.bar.com \
  -e SMTP_USERNAME=foo@bar.com \
  -e SMTP_PASSWORD=XXXXXXXX \
  -e SERVER_HOSTNAME=mail.example.com \
  -e RELAY_NETWORKS=10.260.12.0/24 10.242.8.0/24 \
  registry.gitlab.com/jebr/docker-postfix:1.1
```

Optional environment variables

* `SMTP_PORT` - port for the container to communicate with the relayhost on (default value: 587)
* `UPSTREAM_PROXY` - upstream proxy protocol to use

