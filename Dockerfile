FROM alpine:edge

RUN \
 echo "**** install packages ****" && \
  apk add --no-cache --upgrade \
  bash \
  rsyslog \
  supervisor \
  cyrus-sasl \
  cyrus-sasl-plain \
  cyrus-sasl-crammd5 \
  cyrus-sasl-digestmd5 \
  mailx \
  postfix 
RUN sed -i -e 's/inet_interfaces = localhost/inet_interfaces = all/g' /etc/postfix/main.cf

COPY run.sh /
RUN chmod +x /run.sh
COPY etc/supervisord.conf /etc/supervisord.conf
COPY etc/rsyslog.conf /etc/rsyslog.conf
RUN newaliases

EXPOSE 25
#ENTRYPOINT ["/run.sh"]
CMD ["/run.sh"]
