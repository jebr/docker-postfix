#!/bin/bash

[ "${DEBUG}" == "yes" ] && set -x

 function add_config_value() {
  local key=${1}
  local value=${2}
  local config_file=${3:-/etc/postfix/main.cf}
  [ "${key}" == "" ] && echo "ERROR: No key set !!" && exit 1
  [ "${value}" == "" ] && echo "ERROR: No value set !!" && exit 1
  
   echo "Setting configuration option ${key} with value: ${value}"
  sed -i -e "/^#\?\(\s*${key}\s*=\s*\).*/{s//\1${value}/;:a;n;:ba;q}" \
         -e "\$a${key}=${value}" \
         ${config_file}
}

[ -z "${SMTP_SERVER}" ] && echo "SMTP_SERVER is not set" && exit 1
[ -z "${SMTP_USERNAME}" ] && echo "SMTP_USERNAME is not set" && exit 1
[ -z "${SMTP_PASSWORD}" ] && echo "SMTP_PASSWORD is not set" && exit 1
[ -z "${SERVER_HOSTNAME}" ] && echo "SERVER_HOSTNAME is not set" && exit 1
[ -z "${RELAY_NETWORKS}" ] && echo "RELAY_NETWORKS is not set" && exit 1

SMTP_PORT="${SMTP_PORT-587}"

# Get the domain from the server host name
DOMAIN=`echo ${SERVER_HOSTNAME} | awk -F'.' 'BEGIN{OFS=".";} {$1="";} {print $0;}' | sed 's/^.//'`

# Set needed config options
add_config_value "myhostname" ${SERVER_HOSTNAME}
add_config_value "mydomain" ${DOMAIN}
add_config_value "mydestination" '$myhostname'
add_config_value "myorigin" '$mydomain'
add_config_value "relayhost" "[${SMTP_SERVER}]:${SMTP_PORT}"
add_config_value "smtp_use_tls" "yes"
add_config_value "smtp_sasl_auth_enable" "yes"
add_config_value "smtp_sasl_password_maps" "hash:\/etc\/postfix\/sasl_passwd"
add_config_value "smtp_sasl_security_options" "noanonymous"
if [ -n "${UPSTREAM_PROXY}" ]; then
  add_config_value "smtpd_upstream_proxy_protocol" "${UPSTREAM_PROXY}"
fi
add_config_value "smtpd_upstream_proxy_timeout" "50s"
# The sed regex in add_config_value() doesn't like the RELAY_NETWORKS and nothing gets added to the config file.
#add_config_value "mynetworks" "${RELAY_NETWORKS}"

# Adding allowed relay networks the old fashioned way for now
echo "mynetworks = $RELAY_NETWORKS" >> /etc/postfix/main.cf

# Create sasl_passwd file with auth credentials
if [ ! -f /etc/postfix/sasl_passwd ]; then
  grep -q "${SMTP_SERVER}" /etc/postfix/sasl_passwd  > /dev/null 2>&1
  if [ $? -gt 0 ]; then
    echo "Adding SASL authentication configuration"
    echo "[${SMTP_SERVER}]:${SMTP_PORT} ${SMTP_USERNAME}:${SMTP_PASSWORD}" >> /etc/postfix/sasl_passwd
    postmap /etc/postfix/sasl_passwd
  fi
fi

# Start services
exec supervisord -c /etc/supervisord.conf
